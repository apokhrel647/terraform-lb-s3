##################################################################################
# VARIABLES
##################################################################################
variable "key_name" {
  default = "Web-Key"
}
variable "region" {
  default = "us-east-1"
}

variable "cidr_vpc" {
  default = "10.1.0.0/16"
}

variable "createdby_tag" {
  default = "Mr. Jango"
}
variable "environment_tag" {
  default = "Production"
}
variable "bucket_prefix" {
  default = "projecthike837"
}

# How many instances do you want to deploy for NGINX servers?
variable "instance_count" {
  default = 2
}

# How many subnets you want to use for those Instances?
variable "subnet_count" {
  default = 2
}


locals {
  common_tags = {
    CreatedBy   = var.createdby_tag
    Environment = var.environment_tag
    CreatedTime = formatdate("MM-DD-YYYY hh:mm:ss", timestamp())
  }

  s3_bucket = var.bucket_prefix
}